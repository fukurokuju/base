FROM kroniak/ssh-client:3.15
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

ENV TERRAFORM_VERSION=1.2.0
ENV TERRAFORM_ZIPFILE=terraform_${TERRAFORM_VERSION}_linux_amd64.zip
ENV CONFTEST_VERSION=0.32.0

# system packages
RUN apk add --no-cache  \
    curl~=7 \
    ansible~=4

# install terraform
RUN curl -LO "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/${TERRAFORM_ZIPFILE}" \
    && unzip "${TERRAFORM_ZIPFILE}" \
    && mv terraform /usr/local/bin

# add user
RUN adduser -D fuku
USER fuku
WORKDIR /home/fuku
