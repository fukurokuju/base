terraform {
  backend "s3" {
    bucket = "fukurokuju-terraform-state-gitlab-runner"
    key    = "terraform.tfstate"
    region = "eu-west-3"
  }

  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.10"
    }
  }
}


resource "proxmox_lxc" "gitlab-runner" {
  target_node = var.lxc_target_node
  hostname    = var.lxc_hostname
  ostemplate  = var.lxc_ostemplate
  swap        = var.lxc_swap
  memory      = var.lxc_memory
  start       = true
  onboot      = true
  features {
    fuse    = var.lxc_feature_fuse
    nesting = var.lxc_feature_nesting
  }
  rootfs {
    size    = var.lxc_rootfs_size
    storage = var.lxc_rootfs_storage_name
  }
  network {
    name   = var.lxc_network_name
    bridge = var.lxc_network_bridge_name
    ip     = var.lxc_network_ip
    gw     = var.lxc_network_gateway
    ip6    = "auto"
    rate   = var.lxc_network_rate
  }
  ssh_public_keys = var.ssh_public_keys
}
