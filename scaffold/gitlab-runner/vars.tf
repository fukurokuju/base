variable "lxc_target_node" {
  type        = string
  default     = "ramiel"
  description = "PVE node name"
}

variable "lxc_hostname" {
  type        = string
  default     = "gitlab-runner"
  description = "Created LXC hostname"
}

variable "lxc_ostemplate" {
  type        = string
  default     = "storage:vztmpl/debian-11-standard_11.0-1_amd64.tar.gz"
  description = "The name of the lxc template to use when creating a new proxmox_lxc resource"
}

variable "lxc_swap" {
  type        = string
  default     = "2048"
  description = "The amount of swap to allocate"
}

variable "lxc_memory" {
  type        = string
  default     = "1024"
  description = "The amount of memory to allocate"
}

variable "lxc_feature_fuse" {
  type        = string
  default     = "true"
  description = "Enable the fuse filesystem"
}

variable "lxc_feature_nesting" {
  type        = string
  default     = "true"
  description = "Enable nesting"
}

variable "lxc_rootfs_size" {
  type        = string
  default     = "32G"
  description = "The size of the rootfs"
}

variable "lxc_network_name" {
  type        = string
  default     = "eth0"
  description = "The name of the network interface to use"
}

variable "lxc_network_bridge_name" {
  type        = string
  default     = "vmbr0"
  description = "The name of the bridge to use"
}

variable "lxc_network_ip" {
  type        = string
  default     = "192.168.3.118/24"
  description = "The ip address to use"
}

variable "lxc_network_gateway" {
  type        = string
  default     = "192.168.3.1"
  description = "The gateway to use"
}

variable "lxc_network_rate" {
  type        = string
  default     = "500"
  description = "The network rate to use, in MB/s"
}

variable "lxc_rootfs_storage_name" {
  type        = string
  default     = "storage"
  description = "The name of the storage to use for the rootfs"
}

variable "ssh_public_keys" {
  type        = string
  default     = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE6DCfrklPMoAh8w2V7TfuE81upjJaXvlCV/A0doE/3s catalin@roboces.dev
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGreLVacZyxq2EtgievpXgC/L7czKyJa/kWpgqDoPgnA phireh@fukurokuju.dev
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1aEkUVzYhn+Bs1e8VWjjel1C56Bj1RPoFkt9E9FCdu runner@gitlab
  EOT
  description = "The public ssh keys to use"
}