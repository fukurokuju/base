terraform {
  backend "s3" {
    bucket = "fukurokuju-terraform-state-k3s-testing"
    key    = "terraform.tfstate"
    region = "eu-west-3"
  }

  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.10"
    }
  }
}

resource "proxmox_vm_qemu" "k3s-testing" {
  count       = 1
  vmid        = 1119
  name        = "k3s-testing"
  clone       = "ubuntu-2004-cloudinit-template"
  target_node = "ramiel"
  agent       = 1
  os_type     = "cloud-init"
  cores       = 2
  sockets     = 1
  cpu         = "host"
  memory      = 2048
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  onboot      = true
  disk {
    slot     = 0
    size     = "20G"
    type     = "scsi"
    storage  = "storage"
    iothread = 1
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  ipconfig0 = "ip=192.168.3.119/24,gw=192.168.3.1"
  sshkeys   = <<EOF
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGreLVacZyxq2EtgievpXgC/L7czKyJa/kWpgqDoPgnA phireh@fukurokuju.dev
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII8LvY/0JHgBNnVG9kTQfxmyTPq0pyQRbFOZqFPpIHl6 catalin@roboces.dev
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1aEkUVzYhn+Bs1e8VWjjel1C56Bj1RPoFkt9E9FCdu runner@gitlab
  EOF

}
